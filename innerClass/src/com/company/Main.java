package com.company;

class outer1{
 /*Las inner class son un mecanismo de seguridad en Java.
 Sabemos que una clase no se puede asociar con el modificador de acceso private,
 pero si tenemos la clase como un miembro de otra clase, entonces la clase interna puede hacerse privada.
 Y esto también se utiliza para acceder a los miembros privados de una clase.
 Las clases internas son de tres tipos dependiendo de cómo y donde se definen.*/


 private int n = 20;
    //inner Class privada
    private class innerEj1{
        //metodo dentro de inner class
        public void mensaje(){
            System.out.println("Esta es una Inner Class privada");
        }
    }

    /*Accesando mienmros privados usando inner class*/
    public class innerEj2{

        //metodo que obtiene la variable privada n
        public int obtenerNum(){
            return n;
        }
    }

    //Accesando la clase inner desde un metodo
    void mostrarInnerClass(){
        innerEj1 inner = new innerEj1();
        inner.mensaje();
    }
}


class outer2{
    /*En Java, podemos escribir una clase dentro de un método y éste será de tipo local.
    * Al igual que las variables locales, el ámbito de la clase interna está restringido dentro del método.
    * Un método de clase interna local se puede instanciar sólo dentro del método donde se define la clase interna.*/


    //instanciar metodo de clase externa
    void metodo() {
        int n = 30;

        //method-local inner class
        class methodInnerEj {
            public void mensaje() {
                System.out.println("Metodo de la inner class: " + n);
            }
        }


        //llamamos a la innerclass
        methodInnerEj inner = new methodInnerEj();
        inner.mensaje();
    }
}



/*Una inner class declarada sin un nombre de clase es conocida como una inner class anónima.
* En el caso de clases internas anónimas, las declaramos e instanciamos al mismo tiempo.
 * Por lo general, se utilizan cuando se necesita para sobreescribir el método de una clase o una interfaz.*/
abstract class innerAnonimo{
    public abstract void metodo();
}


class outer3{
    /*Una clase interna estática es una clase anidada que es un miembro estático de la clase externa.
    * Se puede acceder sin instanciar la clase externa, utilizando otros miembros estáticos.
    * Al igual que los miembros estáticos, una clase anidada estática no tiene acceso a las variables
    * de instancia y los métodos de la clase externa.*/
    static class nestedEj{
        public void mensaje(){
            System.out.println("Este es el ejemplo de una static nested class");
        }
    }
}


public class Main {

    public static void main(String[] args) {
	    //inicializando clase esxterna outer1
        outer1 out1 = new outer1();

        //Accesando metodo que llama clase Innner
        out1.mostrarInnerClass();

        //Accesadno la clase inner dentro de outer1
        outer1.innerEj2 inn = out1.new innerEj2();
        System.out.println("Valor entero privado de outer: "+ inn.obtenerNum());


        System.out.println();

        //inicilizando clase externa outer2
        outer2 out2 = new outer2();
        out2.metodo();

        System.out.println();

        //llamando inner anonimo y sobreescribirlo
        innerAnonimo inAn = new innerAnonimo() {
            @Override
            public void metodo() {
                System.out.println("Ejemplo inner Anonimo");
            }
        };
        //llamando metodo sobreescrito
        inAn.metodo();

        System.out.println();

        //inicializando clase anidada interna dentro de outer3
        outer3.nestedEj outNest = new outer3.nestedEj();
        outNest.mensaje();


    }
}
